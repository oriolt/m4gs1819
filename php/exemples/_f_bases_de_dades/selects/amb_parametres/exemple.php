<?php
session_start();

$server = "localhost";
$username = "webuser";
$password = "super3";
$db = "hotel";

try {
  // Comprovació de paràmetres
  if (!isset($_POST['firstname']) || !isset($_POST['lastname'])) {
    throw new Exception("Falten paràmetres.");
  }
  $firstname = trim($_POST['firstname']);
  $lastname = trim($_POST['lastname']);
  if ($firstname=='') {
    $firstname='%';
  }
  if ($lastname=='') {
    $lastname='%';
  }

  $conn = new PDO("mysql:host=$server;dbname=$db;charset=utf8", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $statement = $conn->prepare("SELECT Id, FirstName, LastName FROM Hosts WHERE FirstName LIKE :firstname AND LastName LIKE :lastname");
  $statement->bindParam(':firstname', $firstname);
  $statement->bindParam(':lastname', $lastname);
  $statement->execute();
  $hosts = $statement->fetchAll();
} catch(PDOException $e) {
  $_SESSION['error'] = "No s'ha pogut recuperar la llista de clients:\n{$e->getMessage()}\n";
  header('Location: index.php');
  exit();
} catch (Exception $e) {
  $_SESSION['error'] = $e->getMessage();
  header('Location: index.php');
  exit();
}
?>
<!DOCTYPE html>
<html lang="ca">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <title>SELECT amb paràmetres</title>
  </head>
  <body>
    <main role="main" class="container">
      <h1 class="mt-5">Hostes</h1>
      <?php
      if (sizeof($hosts)>0) {
        echo "<table class='table table-striped'>\n<tr><th>Id</th><th>Nom</th><th>Cognom</th></tr>\n";
        foreach ($hosts as $host) {
          echo "<tr><td>{$host['Id']}</td><td>{$host['FirstName']}</td><td>{$host['LastName']}</td></tr>\n";
        }
        echo "</table>\n";
      } else {
        echo "<p>No hi ha hostes que compleixin el criteri de cerca.</p>\n";
      }
      ?>
    </main>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>
