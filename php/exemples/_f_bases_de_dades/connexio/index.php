<?php
$server = "localhost";
$username = "webuser";
$password = "super3";
$db = "hotel";

try {
   $conn = new PDO("mysql:host=$server;dbname=$db;charset=utf8", $username, $password);
   $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   echo "Connexió exitosa";
} catch(PDOException $e) {
   echo "Ha fallat la connexió: " . $e->getMessage();
}
?>
