<!DOCTYPE html>
<html lang="ca">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <title> Exemple (FOR) d'estructures php </title>
  </head>
  <body>
    <main>
   <p>Mostrar una taula de 4 per 4 que mostri les primeres 4 potències dels números de l’1 al 4 (fer una funció que les calculi invocant la funció pow). </p>
      <?php				
        define("GRAN",4);
        function potencia ($v1, $v2)
         {			
          $res= pow($v1, $v2);		
          return $res;
         }
        echo "<table border=1>";
        for ($n1=1; $n1<=GRAN; $n1++)
         {
          echo "<tr>";
          for ($n2=1; $n2<=GRAN; $n2++)
               echo "<td>". potencia($n1,$n2). "</td>";
          echo "</tr>";
         }
        echo "</table>";
        ?>		
    </main>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>