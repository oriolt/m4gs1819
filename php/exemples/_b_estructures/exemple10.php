<!DOCTYPE html>
<html lang="ca">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <title> Exemple (FOR i IF) de condicionals de php </title>
  </head>
  <body>
    <main>
        <p>Mostra per pantalla una taula de 10 per 10 amb els números de l’1 al 100 però colorejant les fileres alternant gris i blanc. </p>
      <?php
define("GRAND",10);
echo "<table border=1>";
$n=1;
for ($n1=1; $n1<=GRAND; $n1++)
{					
  if ($n1 % 2 == 0)
      echo "<tr bgcolor=#bdc3d6>";
  else
      echo "<tr>";		
  for ($n2=1; $n2<=GRAND; $n2++)
   {				
     echo "<td>", $n, "</td>";
     $n=$n+1;
   }
  echo "</tr>";
 }					
echo "</table>";
 ?>
    </main>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>
